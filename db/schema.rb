# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_27_211146) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.integer "max"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ancestry"
    t.string "namespace"
    t.index ["ancestry"], name: "index_categories_on_ancestry"
  end

  create_table "indications", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "indicado"
    t.string "latitude"
    t.string "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "cpf"
    t.boolean "status"
    t.string "codigo"
    t.index ["user_id"], name: "index_indications_on_user_id"
  end

  create_table "projeto_duvidas", force: :cascade do |t|
    t.bigint "projeto_id"
    t.text "duvida"
    t.text "resposta"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["projeto_id"], name: "index_projeto_duvidas_on_projeto_id"
    t.index ["user_id"], name: "index_projeto_duvidas_on_user_id"
  end

  create_table "projeto_opcaos", force: :cascade do |t|
    t.string "nome"
    t.bigint "projeto_id"
    t.integer "votos"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["projeto_id"], name: "index_projeto_opcaos_on_projeto_id"
  end

  create_table "projeto_users", force: :cascade do |t|
    t.bigint "projeto_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["projeto_id"], name: "index_projeto_users_on_projeto_id"
    t.index ["user_id"], name: "index_projeto_users_on_user_id"
  end

  create_table "projetos", force: :cascade do |t|
    t.string "nome"
    t.text "descricao"
    t.string "video"
    t.bigint "user_id"
    t.boolean "aprovado"
    t.datetime "data_inicio"
    t.datetime "data_termino"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_projetos_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "nome"
    t.string "cpf"
    t.string "genero"
    t.bigint "category_id"
    t.integer "indicador"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "ancestry"
    t.date "data_nascimento"
    t.boolean "admin"
    t.string "cidade"
    t.string "estado"
    t.string "telefone"
    t.string "estado_votacao"
    t.string "cidade_votacao"
    t.boolean "active"
    t.string "codigo_indicacao"
    t.text "texto_candidato"
    t.text "video_candidato"
    t.integer "compartilhado_id"
    t.index ["ancestry"], name: "index_users_on_ancestry"
    t.index ["category_id"], name: "index_users_on_category_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "votes", force: :cascade do |t|
    t.bigint "projeto_opcao_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["projeto_opcao_id"], name: "index_votes_on_projeto_opcao_id"
  end

  add_foreign_key "indications", "users"
  add_foreign_key "projeto_duvidas", "projetos"
  add_foreign_key "projeto_duvidas", "users"
  add_foreign_key "projeto_opcaos", "projetos"
  add_foreign_key "projeto_users", "projetos"
  add_foreign_key "projeto_users", "users"
  add_foreign_key "projetos", "users"
  add_foreign_key "users", "categories"
  add_foreign_key "votes", "projeto_opcaos"
end
