class AddCpfToIndication < ActiveRecord::Migration[5.2]
  def change
    add_column :indications, :cpf, :string
  end
end
