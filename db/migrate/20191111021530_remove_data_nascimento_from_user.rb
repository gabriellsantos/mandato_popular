class RemoveDataNascimentoFromUser < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :data_nascimento, :string
  end
end
