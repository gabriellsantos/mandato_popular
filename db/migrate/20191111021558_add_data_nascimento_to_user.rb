class AddDataNascimentoToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :data_nascimento, :date
  end
end
