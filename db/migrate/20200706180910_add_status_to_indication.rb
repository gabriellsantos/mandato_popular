class AddStatusToIndication < ActiveRecord::Migration[5.2]
  def change
    add_column :indications, :status, :boolean
    add_column :indications, :codigo, :string
  end
end
