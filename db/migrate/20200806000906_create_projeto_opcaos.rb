class CreateProjetoOpcaos < ActiveRecord::Migration[5.2]
  def change
    create_table :projeto_opcaos do |t|
      t.string :nome
      t.references :projeto, foreign_key: true
      t.integer :votos

      t.timestamps
    end
  end
end
