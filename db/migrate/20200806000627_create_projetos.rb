class CreateProjetos < ActiveRecord::Migration[5.2]
  def change
    create_table :projetos do |t|
      t.string :nome
      t.text :descricao
      t.string :video
      t.references :user, foreign_key: true
      t.boolean :aprovado
      t.datetime :data_inicio
      t.datetime :data_termino

      t.timestamps
    end
  end
end
