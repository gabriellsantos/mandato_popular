class AddCidadeToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :cidade, :string
    add_column :users, :estado, :string
  end
end
