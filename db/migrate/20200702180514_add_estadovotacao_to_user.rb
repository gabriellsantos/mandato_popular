class AddEstadovotacaoToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :estado_votacao, :string
    add_column :users, :cidade_votacao, :string
  end
end
