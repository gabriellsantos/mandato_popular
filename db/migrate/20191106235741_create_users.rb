class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :nome
      t.string :cpf
      t.string :data_nascimento
      t.string :genero
      t.references :category, foreign_key: true
      t.integer :indicador

      t.timestamps
    end
  end
end
