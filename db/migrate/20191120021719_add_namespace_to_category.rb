class AddNamespaceToCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :namespace, :string
  end
end
