class CreateProjetoDuvidas < ActiveRecord::Migration[5.2]
  def change
    create_table :projeto_duvidas do |t|
      t.references :projeto, foreign_key: true
      t.text :duvida
      t.text :resposta
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
