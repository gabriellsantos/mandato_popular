desc "Notifica os P30 sobre eleitores sem rede"
task :eleitores_sem_rede => :environment do

  if Date.today.wday == 1 #segunda feira
    User.eleitores_sem_rede
  end

end