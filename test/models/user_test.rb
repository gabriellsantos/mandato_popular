# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  active                 :boolean
#  admin                  :boolean
#  ancestry               :string
#  cidade                 :string
#  cidade_votacao         :string
#  codigo_indicacao       :string
#  cpf                    :string
#  data_nascimento        :date
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  estado                 :string
#  estado_votacao         :string
#  genero                 :string
#  indicador              :integer
#  nome                   :string
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  telefone               :string
#  texto_candidato        :text
#  video_candidato        :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  category_id            :bigint
#  compartilhado_id       :integer
#
# Indexes
#
#  index_users_on_ancestry              (ancestry)
#  index_users_on_category_id           (category_id)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
