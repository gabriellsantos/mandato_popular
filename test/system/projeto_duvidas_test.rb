require "application_system_test_case"

class ProjetoDuvidasTest < ApplicationSystemTestCase
  setup do
    @projeto_duvida = projeto_duvidas(:one)
  end

  test "visiting the index" do
    visit projeto_duvidas_url
    assert_selector "h1", text: "Projeto Duvidas"
  end

  test "creating a Projeto duvida" do
    visit projeto_duvidas_url
    click_on "New Projeto Duvida"

    fill_in "Duvida", with: @projeto_duvida.duvida
    fill_in "Projeto", with: @projeto_duvida.projeto_id
    fill_in "Resposta", with: @projeto_duvida.resposta
    fill_in "User", with: @projeto_duvida.user_id
    click_on "Create Projeto duvida"

    assert_text "Projeto duvida was successfully created"
    click_on "Back"
  end

  test "updating a Projeto duvida" do
    visit projeto_duvidas_url
    click_on "Edit", match: :first

    fill_in "Duvida", with: @projeto_duvida.duvida
    fill_in "Projeto", with: @projeto_duvida.projeto_id
    fill_in "Resposta", with: @projeto_duvida.resposta
    fill_in "User", with: @projeto_duvida.user_id
    click_on "Update Projeto duvida"

    assert_text "Projeto duvida was successfully updated"
    click_on "Back"
  end

  test "destroying a Projeto duvida" do
    visit projeto_duvidas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Projeto duvida was successfully destroyed"
  end
end
