require 'test_helper'

class ProjetoDuvidasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @projeto_duvida = projeto_duvidas(:one)
  end

  test "should get index" do
    get projeto_duvidas_url
    assert_response :success
  end

  test "should get new" do
    get new_projeto_duvida_url
    assert_response :success
  end

  test "should create projeto_duvida" do
    assert_difference('ProjetoDuvida.count') do
      post projeto_duvidas_url, params: { projeto_duvida: { duvida: @projeto_duvida.duvida, projeto_id: @projeto_duvida.projeto_id, resposta: @projeto_duvida.resposta, user_id: @projeto_duvida.user_id } }
    end

    assert_redirected_to projeto_duvida_url(ProjetoDuvida.last)
  end

  test "should show projeto_duvida" do
    get projeto_duvida_url(@projeto_duvida)
    assert_response :success
  end

  test "should get edit" do
    get edit_projeto_duvida_url(@projeto_duvida)
    assert_response :success
  end

  test "should update projeto_duvida" do
    patch projeto_duvida_url(@projeto_duvida), params: { projeto_duvida: { duvida: @projeto_duvida.duvida, projeto_id: @projeto_duvida.projeto_id, resposta: @projeto_duvida.resposta, user_id: @projeto_duvida.user_id } }
    assert_redirected_to projeto_duvida_url(@projeto_duvida)
  end

  test "should destroy projeto_duvida" do
    assert_difference('ProjetoDuvida.count', -1) do
      delete projeto_duvida_url(@projeto_duvida)
    end

    assert_redirected_to projeto_duvidas_url
  end
end
