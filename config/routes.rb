Rails.application.routes.draw do
  resources :projeto_duvidas
  resources :projetos do
    member do
      get "aprovar_projeto"
    end
  end
  devise_for :users
  resources :users do
    collection do
      post "registrar_via_app"
      post "autenticar_via_app"
      post "indicar"
      get "receber_indicacao"
      get "send_firebase_message"
      post "recuperar_senha"
      get "confirmar_cadastro"
      post "indicar_nao_membro"
      post "atualizar_dados"
      post "atualizar_senha"
      post "cadastrar_projeto"
      get "carregar_projetos"
      post "votar"
      post "cadastrar_duvida"
      post "responder_duvida"
      get "retornar_duvidas"
      get "carregar_p30"
      post "compartilhar_rede"
      post "remover_compartilhamento"


    end
    member do
      get "rede"
      get "atualizar_usuario"
    end
  end
  resources :categories do
    collection do
      get "add_children"
      get "atualiza_pai"
    end
  end

  root "categories#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
