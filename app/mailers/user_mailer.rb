#Classe que controla os métodos de envio de e-mail
class UserMailer < ApplicationMailer
  #Método que envia os e-mail com os conteúdos
  def enviar_senha(senha,user)
    @user = user
    @senha = senha
    mail(to: @user.email, subject: 'Recuperação de senha')
  end

  #Método que envia os e-mail para a confirmação do cadastro
  def confirmar_cadastro
    @user = params[:user]
    mail(to: @user.email, subject: 'Confirme seu cadastro')
  end

  #Método que envia os e-mail com a indicação para quem ainda não é membro
  def enviar_indicacao
    @user = params[:user]
    @indicacao = params[:indicacao]
    mail(to: params[:email], subject: 'Indicação Mandato Popular')
  end

  #Método que envia os e-mail para membros já cadastrados
  def enviar_indicacao_membro
    @user = params[:user]
    @indicacao = params[:indicacao]
    mail(to: params[:email], subject: 'Indicação Mandato Popular')
  end

  #Método que envia e-mail para os P30 sobre os eleitores sem Rede
  def eleitores_sem_rede
    @user = params[:user]
    @eleitores = params[:eleitores]
    mail(to: @user.email, subject: 'Eleitores sem rede')
  end
end