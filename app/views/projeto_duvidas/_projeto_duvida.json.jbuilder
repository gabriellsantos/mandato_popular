json.extract! projeto_duvida, :id, :projeto_id, :duvida, :resposta, :user_id, :created_at, :updated_at
json.url projeto_duvida_url(projeto_duvida, format: :json)
