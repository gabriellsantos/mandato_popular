json.extract! user, :id, :nome, :cpf, :data_nascimento, :genero, :category_id, :indicador, :created_at, :updated_at
json.url user_url(user, format: :json)
