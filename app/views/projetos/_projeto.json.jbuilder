json.extract! projeto, :id, :nome, :descricao, :video, :user_id, :aprovado, :data_inicio, :data_termino, :created_at, :updated_at
json.url projeto_url(projeto, format: :json)
