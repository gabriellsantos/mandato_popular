class ProjetoDuvidasController < ApplicationController
  before_action :set_projeto_duvida, only: [:show, :edit, :update, :destroy]

  # GET /projeto_duvidas
  # GET /projeto_duvidas.json
  def index
    @projeto_duvidas = ProjetoDuvida.all
  end

  # GET /projeto_duvidas/1
  # GET /projeto_duvidas/1.json
  def show
  end

  # GET /projeto_duvidas/new
  def new
    @projeto_duvida = ProjetoDuvida.new
  end

  # GET /projeto_duvidas/1/edit
  def edit
  end

  # POST /projeto_duvidas
  # POST /projeto_duvidas.json
  def create
    @projeto_duvida = ProjetoDuvida.new(projeto_duvida_params)

    respond_to do |format|
      if @projeto_duvida.save
        format.html { redirect_to @projeto_duvida, notice: 'Projeto duvida was successfully created.' }
        format.json { render :show, status: :created, location: @projeto_duvida }
      else
        format.html { render :new }
        format.json { render json: @projeto_duvida.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projeto_duvidas/1
  # PATCH/PUT /projeto_duvidas/1.json
  def update
    respond_to do |format|
      if @projeto_duvida.update(projeto_duvida_params)
        format.html { redirect_to @projeto_duvida, notice: 'Projeto duvida was successfully updated.' }
        format.json { render :show, status: :ok, location: @projeto_duvida }
      else
        format.html { render :edit }
        format.json { render json: @projeto_duvida.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projeto_duvidas/1
  # DELETE /projeto_duvidas/1.json
  def destroy
    @projeto_duvida.destroy
    respond_to do |format|
      format.html { redirect_to projeto_duvidas_url, notice: 'Projeto duvida was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_projeto_duvida
      @projeto_duvida = ProjetoDuvida.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def projeto_duvida_params
      params.require(:projeto_duvida).permit(:projeto_id, :duvida, :resposta, :user_id)
    end
end
