class CategoriesController < ApplicationController


  before_action :authenticate_user!
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.all#.where(active: true)
  end



  # GET /categories/1
  # GET /categories/1.json
  def show
  end

  # GET /categories/new
  def new
    @category = Category.new(active: true)
  end

  # GET /categories/1/edit
  def edit
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to @category, notice: 'Categoria cadastrada com sucesso.' }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  #Adiciona uma catogia filha
  def add_children
    categoria = Category.find(params[:category_id])
    Category.where(id: categoria.child_ids).update_all(active: false)
    categoria.children.create name: params[:name], description: params[:description], max: params[:max], active: params[:active]
    redirect_to categories_path
  end

  #Atualiza o pai da categoria selecionada
  def atualiza_pai
    categoria_filha = Category.find(params[:categoria_filha])
    categoria_pai = Category.find(params[:categoria_pai])
    categoria_filha.update_attribute :parent, categoria_pai
    redirect_to categories_path
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    #binding.pry
    pai = @category.parent
    #if pai.present?
    #if params[:category][:active].present? and params[:category][:active] == "1"
     # Category.where(id: pai.child_ids).update_all(active: false)
    #end
    #end
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to @category, notice: 'Categoria atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category.destroy
    respond_to do |format|
      format.html { redirect_to categories_url, notice: 'Categoria excluída com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name, :description, :max, :active,:namespace)
    end
end
