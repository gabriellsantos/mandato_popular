class UsersController < ApplicationController
  require 'fcm'
  skip_before_action :verify_authenticity_token

  before_action :allow_iframe, :set_headers
  before_action :authenticate_user!, except: [:remover_compartilhamento,:compartilhar_rede,:carregar_p30,:retornar_duvidas,:responder_duvida,:cadastrar_duvida,:votar,:carregar_projetos,:cadastrar_projeto,:atualizar_dados, :indicar_nao_membro,:confirmar_cadastro,:registrar_via_app, :autenticar_via_app,:indicar,:receber_indicacao,:recuperar_senha]
  before_action :set_user, only: [:show, :edit, :update, :destroy,:rede,:atualizar_usuario]
  before_action :set_combos, only: [:new,:edit,:update,:create]


  #Método utilizado para desfazer o compartilhamento da rede
  def remover_compartilhamento
    user = User.find_by_cpf(params[:cpf])
    user.parent = nil
    user.compartilhar_socify(user.compartilhado_id,false)
    user.compartilhado_id = nil
    if user.save
      render json: {status: "OK", msg: "O compartilhamento foi removido", compartilhado_id: 0, compartilhado_nome: ""}
    else
      render json: {status: "ERROR", msg: user.errors.full_message.to_sentence}
    end
  end

  #Método utlizado para compartilhar a rede do candidato
  def compartilhar_rede
    user = User.find_by_cpf(params[:cpf])
    p30_selecionado = JSON.parse(params[:p30])
    p30 = User.find(p30_selecionado["id"])
    user.parent = p30
    user.compartilhado_id = p30.id
    if user.save
      user.compartilhar_socify(p30.id,true)
      render json: {status: "OK", msg: "Sua rede foi compartilhada com #{p30.nome}", compartilhado_id: p30.id, compartilhado_nome: p30.nome}
    else
      render json: {status: "ERROR", msg: user.errors.full_message.to_sentence}
    end
  end

  #Método que retorna os p30 para o compartilhamento de rede
  def carregar_p30
    users = User.includes(:category).where(categories: {namespace:  "p30"}).where.not(cpf: params[:cpf])
    render json: {status: "OK", p30: users}
  end

  #Método que retorna as dúvidas do projeto
  def retornar_duvidas
    duvidas = ProjetoDuvida.where(projeto_id: params[:projeto])
    render json: {status: "OK", duvidas: duvidas}
  end

  #Método para criar a dúvida do projeto
  def cadastrar_duvida
    user = User.find_by_cpf(params[:cpf])
    duvida = ProjetoDuvida.new(duvida_params)
    duvida.user = user
    duvida.resposta = ""
    if duvida.save
      render json: {status: "OK", msg: "Dúvida enviada com sucesso.", duvida: duvida}
    else
      render json: {status: "ERROR", msg: duvida.errors.full_message.to_sentence }
    end

  end

  #Método utilizado para responder a dúvida
  def responder_duvida
    duvida = ProjetoDuvida.find(params[:id])
    if duvida.update(duvida_params)
      duvidas = ProjetoDuvida.where(projeto_id: duvida.projeto_id)
      render json: {status: "OK", msg: "Resposta enviada com sucesso.", duvidas: duvidas}
    else
      render json: {status: "ERROR", msg: duvida.errors.full_message.to_sentence }
    end


  end


  #Método que atualiza os dados cadastrais
  def atualizar_dados
    #binding.pry
    @user = User.find_by_cpf(params[:cpf])
    if @user.update(user_params2)
      render json: {status: "OK", msg: "Dados atualizados com sucesso!"}
    else
      render json: {status: "error", msg: @user.errors}
    end
  end

  #método utilizado para realizar a votação no projeto
  def votar
    user = User.find_by_cpf(params[:cpf])
    opcao = ProjetoOpcao.find(params[:opcao])
    votou = ProjetoUser.where(user: user, projeto_id: opcao.projeto_id).last

    if votou.present?
      render json: {status: "ERROR", msg: "Você já votou neste projeto!"}
    else
      Vote.create(projeto_opcao_id: opcao.id)
      ProjetoUser.create(user_id: user.id, projeto_id: opcao.projeto_id)
      render json: {status: "OK", msg:"Voto realizado com sucesso!"}
    end


  end


  #Métoo carrega os projetos
  def carregar_projetos
    user = User.find_by_cpf(params[:cpf])
    if params[:tipo] == "p30"
      projetos = Projeto.where(user: user)
    else
      if user.root.category.namespace == "p30"
        projetos = Projeto.where(user: user.root, aprovado: true)
      else
        projetos = []
      end

    end

    render json: {status:"OK",projetos: projetos}


  end


  #Método que confirma o cadastro por email do usuário
  def confirmar_cadastro
      user = User.find(params[:code])
      user.active = true
      user.save
      render layout: "clean"
  end


  #Método que envia uma nova senha para o email do usuário
  def recuperar_senha
    user = User.find_by_cpf(params[:cpf])
    if user.present?
      nova_senha = (0...6).map { rand(10) }.join
      user.password = nova_senha
      user.password_confirmation = nova_senha
      if user.save
        UserMailer.enviar_senha(nova_senha,user).deliver
        render json: {status:"OK", msg: "Um e-mail com sua nova senha foi enviado para #{user.email}"}
      else
        render json: {status:"ERROR", msg: user.errors.full_messages.to_sentence}
      end
    else
      render json: {status:"ERROR", msg: "Usuário não encontrado"}
    end


  end

  #Envia o push notification
  def send_firebase_message
    fcm = FCM.new("AAAAuKwVM2c:APA91bEqwWboRl9cPyAdkp20oIu_Jgh8OPbfgJprsmydAiGrAJVrUUhYOvCulEfV5-TVI4tMMBzDlxxQvvv_otKezPvZoH-cy06p99zqIeOEkWUmyDoupMscs27_0R6OMwbs2wIVfOdT")
    options = { "notification": {
        "title": params[:titulo],
        "body": params[:msg]
    }
    }
    response = fcm.send_to_topic(params[:topico], options)
    redirect_to users_path, notice: "Notificação enviada com sucesso."
  end

  # GET /users
  # GET /users.json
  def index
    @q = User.ransack(params[:q])
    @users = @q.result.page(params[:page]).per(15)
    @topicos = Category.all.map{|a| [a.name,a.namespace]}
    @topicos << ["Geral","geral"]
  end

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end

  def set_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'GET'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  end


  #Método utilizado pra realizar o cadastro de um projeto para votação
  def cadastrar_projeto
    #binding.pry
    user = User.find_by_cpf(params[:cpf])
    projeto = Projeto.new(user: user)
    projeto.aprovado = false
    projeto.data_inicio = params[:data_inicio]
    projeto.data_termino = params[:data_termino]
    projeto.descricao = params[:descricao]
    projeto.nome = params[:nome]
    projeto.video = params[:video]
    if projeto.save
      opcoes = JSON.parse(params[:opcoes])
      opcoes.each do |o|
        ProjetoOpcao.create(projeto: projeto, votos: 0, nome: o["opcao"])
      end
      render json: {status: "OK", projeto: projeto, msg: "Projeto cadastrado com sucesso!"}
    else
      render json: {status: "ERROR", msg: projeto.errors.full_messages.to_sentence}
    end

  end

  #Método utilizado para registrar o usuário pelo app
  def registrar_via_app

    categoria = Category.find_by_namespace(params[:tipo])
    user = User.new(codigo_indicacao: params[:indicacao], cidade_votacao: params[:cidade_votacao], estado_votacao: params[:estado_votacao],cidade: params[:cidade], estado: params[:estado],telefone: params[:telefone],category_id: categoria.id, nome: params[:nome], cpf: params[:cpf], email: params[:email], genero: params[:genero], data_nascimento: params[:data_nascimento], password: params[:password], password_confirmation:params[:password_confirmation])
    user.active = false
    #binding.pry
    if user.save
      UserMailer.with(user: user).confirmar_cadastro.deliver_later
      render json: {status: "OK", user: user}
    else
      render json: {status: "ERROR", msg: user.errors.full_messages.to_sentence}
    end

  end

  #Método utilizado para autenticar pelo app'
  def autenticar_via_app
    user = User.find_by(cpf: params[:cpf])

    if user.present? and user.active
      if user.valid_password? params[:password]
        if user.root != user
          render json: {texto_candidato: user.texto_candidato,video_candidato: user.video_candidato,status: "OK", user: user, qtd_membros: user.root.descendants.size, qtd_abaixo: user.descendants.size, qtd_cidade: User.where(cidade: user.cidade).count, candidato: user.root.category.namespace == "p30" ? user.root.nome : "Sem Candidato"}
        else
          render json: {texto_candidato: user.texto_candidato,video_candidato: user.video_candidato,status: "OK", user: user, qtd_membros: user.root.descendants.size, qtd_abaixo: user.descendants.size, qtd_cidade: User.where(cidade: user.cidade).count, candidato: user.category.namespace == "p30" ? user.nome : "Sem Candidato"}
        end

      else
        render json: {status: "ERROR"}
      end
    else
      render json: {status: "ERROR"}
    end

  end

  #Método utilizado para criar a indicação
  def indicar

    if !(params[:cpf_indicador] == params[:cpf_indicado])
      indicador = User.find_by(cpf: params[:cpf_indicador])
      indicado = User.find_by(cpf: params[:cpf_indicado])
      if indicador.category.present?
        if User.where(indicador: indicador.id).count < indicador.category.max
          if !indicado.present?
            render json: {status: "ERROR", msg:"Indicado precisa estar cadastrado antes de realizar a indicação."}
          else
            if indicado.category.present? and indicador.category.child_of?(indicado.category)#Verifica se o indicado já possui uma categoria superiror ao indicador
              render json: {status: "ERROR", msg: "Indicado pertence a uma categoria superior."}
            else
              indication = Indication.new
              indication.user = indicador
              indication.indicado = indicado.id
              indication.latitude = params[:latitude]
              indication.longitude = params[:longitude]
              indication.status = false
              if indication.save
                UserMailer.with(indicacao: indication, user: indicador, email: params[:email]).enviar_indicacao_membro.deliver_later
                render json: {status: "OK", msg:"Indicação realizada com sucesso."}
              else
                render json: {status: "ERROR", msg:"Erro ao criar indicação. Tente novamente."}
              end
            end
          end
        else
          render json: {status: "ERROR", msg: "Você já indicou o máximo permitido."}
        end
      else
        render json: {status: "ERROR", msg: "Você ainda não possui uma categoria."}
      end
    else
      render json: {status: "ERROR", msg: "Você não pode se indicar."}
    end
  end

  #Método utilizado para criar a indicação
  def indicar_nao_membro


      indicador = User.find_by(cpf: params[:cpf_indicador])
      #indicado = User.find_by(cpf: params[:cpf_indicado])
      if indicador.category.present?
        if User.where(indicador: indicador.id).count < indicador.category.max

          indication = Indication.new
          indication.user = indicador
          #indication.cpf = params[:cpf_indicado]
          indication.status = false
          indication.latitude = params[:latitude]
          indication.longitude = params[:longitude]
          if indication.save
            UserMailer.with(indicacao: indication, user: indicador, email: params[:email]).enviar_indicacao.deliver_later
            render json: {status: "OK", msg:"Indicação realizada com sucesso."}
          else
            render json: {status: "ERROR", msg:"Erro ao criar indicação. Tente novamente. #{indication.errors.full_messages.to_sentence}"}
          end

        else
          render json: {status: "ERROR", msg: "Você já indicou o máximo permitido."}
        end
      else
        render json: {status: "ERROR", msg: "Você ainda não possui uma categoria."}
      end

  end

  def rede
    
  end

  #Método que atribui a indicação ao usuário
  def receber_indicacao

    indication = Indication.find_by(codigo: params[:codigo])
    @sucesso = false

    if indication.present? and !indication.status

      indicador = indication.user
      @indicado = User.find(indication.indicado)
      categoria = indicador.category
      categoria_filha = categoria.children.where(active: true).first

      @indicado.indicador = indicador.id
      @indicado.parent = indicador
      if categoria_filha.present?
        @indicado.category = categoria_filha
      else
        @indicado.category = categoria
      end

      if @indicado.save
        @indicado.seguir_rede
        @sucesso = true
      else
        @sucesso = false
      end

    end

    render layout: "clean"
  end

  #Método que atribui a indicação ao usuário
  def receber_indicacao_gps
    user = User.find_by(cpf: params[:cpf])
    latitude = params[:latitude]
    longitude = params[:longitude]
    indication = Indication.where(indicado: user.id).last
    pode_indicar = false

    if indication.present?
      minutos = ((indication.created_at - DateTime.now) / 1.minutes * -1).to_i
      #if minutos < 5 #Verifica se passou 5 minutos
        indicacao = Indication.new(latitude: latitude, longitude: longitude)

        #verifica se o indicador é p30
        if indication.user.category.namespace == "p30"
          #reverse geocode para verificar a cidade do indicador
          res=Geokit::Geocoders::GoogleGeocoder.reverse_geocode "#{indication.latitude}, #{indication.longitude}"
          res2=Geokit::Geocoders::GoogleGeocoder.reverse_geocode "#{latitude}, #{longitude}"
          

          if res.district == res2.district
            pode_indicar = true
          else
            pode_indicar = false
            render json: {status: "ERROR", msg: "Você não está na mesma cidade do Indicador."}
          end
          
        else
          if indicacao.distance_from(indication, unit: :kms) <= 0.5 #Verifica se está a menos de 500m   
            pode_indicar = true
          else
            render json: {status: "ERROR", msg: "Você não está perto do seu indicador."}
          end#verificaçao de 500m
        end


        if pode_indicar
          indicador = indication.user
            categoria = indicador.category
            categoria_filha = categoria.children.where(active: true).first

            user.indicador = indicador.id
            user.parent = indicador
            if categoria_filha.present?
              user.category = categoria_filha
            else
              user.category = categoria
            end

            if user.save
              render json: {status:"OK", user: user}
            else
              render json: {status:"ERROR", msg: user.errors.full_messages.to_sentence}
            end
        end



      #else
        #render json: {status: "ERROR", msg: "Nenhuma indicação ativa."}
      #end

    else
      render json: {status: "ERROR", msg: "Nenhuma indicação ativa."}
    end


  end


# GET /users/1
# GET /users/1.json
  def show
  end

# GET /users/new
  def new
    @user = User.new
  end

# GET /users/1/edit
  def edit

  end

  def atualizar_usuario
    @user.nome = params[:nome] if params[:nome].present?
    @user.genero = params[:genero] if params[:genero].present?
    @user.data_nascimento = params[:data_nascimento] if params[:data_nascimento].present?
    @user.cpf = params[:cpf] if params[:cpf].present?
    if params[:indicador].present?
      @user.indicador = params[:indicador]
      indicador = User.find(params[:indicador])
      @user.parent = indicador
    end

    @user.category_id = params[:category_id] if params[:category_id].present?
    respond_to do |format|
      if @user.save
        format.html { redirect_to users_path, notice: 'Usuário atualizado com sucesso' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

# POST /users
# POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to users_path, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

# PATCH/PUT /users/1
# PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to users_path, notice: 'User was successfully updated.' }
        format.json { render json: {status: "OK", msg: "Dados atualizados com sucesso!"} }
      else
        format.html { render :edit }
        format.json { render json: {status: "error", msg: @user.errors.full_messages.to_sentence}}
      end
    end
  end

# DELETE /users/1
# DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_combos
    @categorias = Category.where(active: true).map{|a| [a.name,a.id]}
    @usuarios = User.all.map{|a| [a.nome,a.id]}
  end
# Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

# Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:texto_candidato,:video_candidato,:nome, :cpf, :data_nascimento, :genero, :category_id, :indicador, :active, :admin, :cidade, :cidade_votacao, :email, :password,:password_confirmation, :estado, :estado_votacao, :telefone)
  end

  def duvida_params
    params.permit(:duvida,:resposta,:projeto_id,:user_id)
  end

  def user_params2
    params.permit(:texto_candidato,:video_candidato,:nome, :cpf, :data_nascimento, :genero, :category_id, :indicador, :active, :admin, :cidade, :cidade_votacao, :email, :password,:password_confirmation, :estado, :estado_votacao, :telefone)
  end
end
