# == Schema Information
#
# Table name: projetos
#
#  id           :bigint           not null, primary key
#  aprovado     :boolean
#  data_inicio  :datetime
#  data_termino :datetime
#  descricao    :text
#  nome         :string
#  video        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :bigint
#
# Indexes
#
#  index_projetos_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class Projeto < ApplicationRecord
  belongs_to :user
  has_many :projeto_opcoes, class_name:"ProjetoOpcao", foreign_key: :projeto_id, primary_key: :id
  has_many :votes, through: :projeto_opcoes

  #Configuração adcional para o to_json
  def as_json(options = {})
    hash = super(options)
    hash[:data_i] = self.data_inicio.to_date.to_s
    hash[:data_f] = self.data_termino.to_date.to_s
    hash[:opcoes] =  self.projeto_opcoes
    hash[:ativo] =  Date.today >= self.data_inicio.to_date and Date.today <= self.data_termino.to_date
    hash
  end

end
