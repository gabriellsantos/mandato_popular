# == Schema Information
#
# Table name: votes
#
#  id               :bigint           not null, primary key
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  projeto_opcao_id :bigint
#
# Indexes
#
#  index_votes_on_projeto_opcao_id  (projeto_opcao_id)
#
# Foreign Keys
#
#  fk_rails_...  (projeto_opcao_id => projeto_opcaos.id)
#

class Vote < ApplicationRecord
  belongs_to :projeto_opcao
end
