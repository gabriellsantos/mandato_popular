# == Schema Information
#
# Table name: indications
#
#  id         :bigint           not null, primary key
#  codigo     :string
#  cpf        :string
#  indicado   :integer
#  latitude   :string
#  longitude  :string
#  status     :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :bigint
#
# Indexes
#
#  index_indications_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#

class Indication < ApplicationRecord
  belongs_to :user
  belongs_to :usuario_indicado, class_name: "User", foreign_key: :indicado, optional: true
  acts_as_mappable :default_units => :kms,
                   :default_formula => :sphere,
                   :lat_column_name => :latitude,
                   :lng_column_name => :longitude

  uniquefy :codigo
end
