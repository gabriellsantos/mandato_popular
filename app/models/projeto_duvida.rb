# == Schema Information
#
# Table name: projeto_duvidas
#
#  id         :bigint           not null, primary key
#  duvida     :text
#  resposta   :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  projeto_id :bigint
#  user_id    :bigint
#
# Indexes
#
#  index_projeto_duvidas_on_projeto_id  (projeto_id)
#  index_projeto_duvidas_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (projeto_id => projetos.id)
#  fk_rails_...  (user_id => users.id)
#

class ProjetoDuvida < ApplicationRecord
  belongs_to :projeto
  belongs_to :user

  #Configuração adcional para o to_json
  def as_json(options = {})
    hash = super(options)
    hash[:usuario] = self.user.nome
    hash
  end
end
