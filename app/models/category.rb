# == Schema Information
#
# Table name: categories
#
#  id          :bigint           not null, primary key
#  name        :string
#  description :string
#  max         :integer
#  active      :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  ancestry    :string
#  namespace   :string
#
# Indexes
#
#  index_categories_on_ancestry  (ancestry)
#

class Category < ApplicationRecord
  has_ancestry



  #Imprime o nome da categoria
  def to_s
    self.name
  end


  #Desativa os outros filhos do pai
  def desativar_filhos
    pai = self.parent
    if self.active == true
      Category.where(id: pai.child_ids).update_all(active: false)
    end
  end
end
