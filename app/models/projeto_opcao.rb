# == Schema Information
#
# Table name: projeto_opcaos
#
#  id         :bigint           not null, primary key
#  nome       :string
#  votos      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  projeto_id :bigint
#
# Indexes
#
#  index_projeto_opcaos_on_projeto_id  (projeto_id)
#
# Foreign Keys
#
#  fk_rails_...  (projeto_id => projetos.id)
#

class ProjetoOpcao < ApplicationRecord
  belongs_to :projeto
  has_many :votes

  #Configuração adcional para o to_json
  def as_json(options = {})
    hash = super(options)
    begin
      hash[:porcentagem] = (100 * self.votes.count) / self.projeto.votes.count
    rescue
      hash[:porcentagem] = 0
    end

    hash
  end


  def to_s
    self.nome
  end

  def total_votos
    self.votes.count
  end
end
