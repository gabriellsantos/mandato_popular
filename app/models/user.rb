# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  active                 :boolean
#  admin                  :boolean
#  ancestry               :string
#  cidade                 :string
#  cidade_votacao         :string
#  codigo_indicacao       :string
#  cpf                    :string
#  data_nascimento        :date
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  estado                 :string
#  estado_votacao         :string
#  genero                 :string
#  indicador              :integer
#  nome                   :string
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  telefone               :string
#  texto_candidato        :text
#  video_candidato        :text
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  category_id            :bigint
#  compartilhado_id       :integer
#
# Indexes
#
#  index_users_on_ancestry              (ancestry)
#  index_users_on_category_id           (category_id)
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#

class User < ApplicationRecord
  require 'rest-client'
  has_ancestry
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  belongs_to :category, optional: true
  has_many :projetos

  validates :email,:cpf, uniqueness: true
  validates :nome, :data_nascimento, :genero,:telefone,:cidade,:estado, :cidade_votacao, :estado_votacao, presence: true

  before_create :set_categoria_eleitor

  after_create :verifica_indicacao
  after_create :cria_usuario_social


  def compartilhar_socify(p30,follow)
    begin
      membros = self.descendant_ids
      result = RestClient.post 'http://mandato-socify.herokuapp.com/users/compartilhar_rede', {follow: follow, user: self.id, p30: p30, membros: membros}
    rescue
    end

  end

  def cria_usuario_social
      result = RestClient.post 'http://mandato-socify.herokuapp.com/users/create_mantado_user', {user: JSON.parse(self.to_json)}
  end

  def seguir_rede
  rede = self.ancestors.pluck(:id)
  result = RestClient.post 'http://mandato-socify.herokuapp.com/users/seguir_rede', {cpf: self.cpf, membros: rede}
  end

  def self.atualizar_usuarios
    User.where(estado_votacao: nil).each do |u|
      begin
      u.estado_votacao = u.estado
      u.cidade_votacao = u.cidade
      u.active = true
      u.save
      rescue
      end
    end
  end

  #Método que notifica os p30 sobre eleitores sem rede na sua zona
  def self.eleitores_sem_rede
    User.where(indicador: nil).group_by(&:cidade_votacao).map do |cidade, users|
      p30 = User.joins(:category).where("categories.namespace ilike 'P30' ").where(cidade_votacao: cidade)
      p30.each do |p|
        UserMailer.with(user: p, eleitores: users).eleitores_sem_rede.deliver_later
      end
    end

  end

  #Método que verifica se existe alguma indicação para o novo membro
  def verifica_indicacao
    if self.codigo_indicacao.present?
      begin
        indicacao = Indication.find_by(codigo: self.codigo_indicacao)
        if indicacao.present? and !indicacao.status
          indicador = indicacao.user
          categoria = indicador.category
          categoria_filha = categoria.children.where(active: true).first

          self.indicador = indicador.id
          self.parent = indicador
          if categoria_filha.present?
            self.category = categoria_filha
          else
            self.category = categoria
          end

          if self.save
            indicacao.update(status: true)
          end
        end
      rescue

      end
    end
  end

  #Seta a categoria eleitor para o primeiro cadastro
  def set_categoria_eleitor
    if !self.category.present?
      self.category = Category.find_by(namespace: "eleitor")
    end
  end

  #Configuração adcional para o to_json
  def as_json(options = {})
    hash = super(options)
    hash[:categoria_nome] =  self.category.present? ? self.category.name : "Sem Categoria"
    hash[:categoria_topico] =  self.category.present? ? self.category.namespace : "eleitor"
    hash
  end

  #Retorna o nome do usuário
  def to_s
    self.nome
  end

  #Verifica se o usuário tem permissão para logar
  def active_for_authentication?
    super and self.admin
  end
end
